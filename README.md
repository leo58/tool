# tool
## git

### feature.sh

在项目目录执行如下命令
`feature.sh 分支注释 [分支名] [父分支]`
分支名默认获取当时时间戳
父分支默认是master
---
例如
feature.sh tool 20190515-v1 ark-qa
---
生成log如下：
```
2019-05-15 16:31:47 (当前项目路径)
git库 add branch name：（feature/20190515-v1） content：tool
```

### log.sh

在项目目录执行如下命令
`log.sh [行数]`
默认是10行
---
例如
log.sh 10
---
内容如下：
```
项目路径
project-name: 项目名

2019-05-08 13:04:17 (项目路径)
git地址 add branch name：（feature/20190508-v1） content：ai小作文推荐

2019-05-08 13:05:09 (项目路径)
git地址 add branch name：（feature/20190508-v1） content：ai小作文推荐

2019-05-15 16:31:47 (项目路径)
git地址 add branch name：（feature/20190515-v1） content：tool
```

## nginx

## other
### del_ago_file.sh
需要写一个文件，里边写好需要检索的目录
`del_ago_file.sh [包含文件目录的文件] [天数]`
param 1 :
传入某一个文件目录，脚本会自动读取该文件中列出的目录，并删除操作指定天数的文件
param 2 :
天数指的是需要删除多少天之前的文件
