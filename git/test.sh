
# 1、检测是否为git项目root目录
function checkIsGitProRoot() {
    if [ ! -d '.git' ];then
        echo '请到git项目根目录下执行。'
        exit
    fi
}

function getShellDir() {
    # shell目录
    # shellDir=$(cd $(dirname $0); pwd)
    shell_dir=`dirname $0`
}

# 获取配置
config_path='config.ini'
function getConfig() {
    getShellDir
    while read line;do
        eval "$line"
    done < $shell_dir/$config_path
}

# 3、check config
function checkConfig() {
    # 1、check log file base path
    if [ ! -d "$log_file_root_path" ];then
        mkdir $log_file_root_path
    fi
}


function getProjectPath() {
    project_path=$(pwd)
}

# 获取当前项目名
function getProjectName() {
    project_name="${project_path##*/}"
}

function setUserParam() {
    # 分支简介
    content=$1
    echo $1
    # 分支简介必填
    if [ -z $content ] ; then
        echo 'please enter content'
        exit 0
    fi
    # 功能分支名
    function_branch=$2
    # 父分支
    if [ -z "$3" ];then
        parent_branch='master'
    else
        parent_branch=$3
    fi
    # 如果没传分支名，设置默认分支名
    if [ -z $function_branch ] ; then
        function_branch=$(date "+%Y%m%d-%H%M%S")
    fi
    branch_name="feature/$function_branch"
}

function setLogFilePath() {
    log_file_base_path="$log_file_root_path/$project_name"
    # 如果log目录不存在，则创建
    if [ ! -d "$log_file_base_path" ];then
        mkdir $log_file_base_path
    fi
    log_file_path="$log_file_base_path/git_branch.log"
}

function getGitUrl() {
    git_url=$(git remote get-url --push origin)
}

function addLog() {
    echo -e "$current_time ($project_path)\r" >> $log_file_path
    echo -e "$git_url add branch name：（$branch_name） content：$content\r" >> $log_file_path
    echo -e "\r" >> $log_file_path
}

function showLog() {
    echo '========== log start ============='
    sh $shell_dir/log.sh 10
    echo '========== log end ============='
}

function pushLog() {
    cd $log_file_root_path
    git pull
    git add .
    git commit -m '$project_name add branch $branch_name content: $content'
    git push
}

function run() {
    # 检测是否在git项目根目录下
    checkIsGitProRoot
    # 获取配置
    getConfig
    # 检测配置
    checkConfig
    # 获取项目路径
    getProjectPath
    # 输出项目路径
    echo $project_path
    # 获取项目名称
    getProjectName
    # 输出项目名
    echo $project_name
    # 设置用户传递的参数
    setUserParam $1 $2 $3
    # 设置log path
    setLogFilePath
    # 切换到 parent_branch
    # git checkout $parent_branch
    # 基于parent_branch 创建功能分支
    # git checkout -b "$branch_name"
    # 将分支信息写入log
    current_time=$(date "+%Y-%m-%d %H:%M:%S")
    # 仓库地址
    getGitUrl
    # 添加log
    addLog
    # 显示log
    showLog
}

run $1 $2 $3
