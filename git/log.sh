if [ -z $2 ];then
    project_path=$(pwd)
    echo $project_path
    project_name="${project_path##*/}"
else
    project_name=$2
fi
echo 'project-name:' $project_name
# shell目录
shellDir=$(cd $(dirname $0); pwd)
# log存放目录
logFileBasePath="$shellDir/$project_name"
# log文件路径
logFilePath="$logFileBasePath/git_branch.log"
if [ -z $1 ];then
    line=10
else
    line=$1
fi
cat $logFilePath | tail -n $line

