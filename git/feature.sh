#!/usr/bin/env bash
# 因为分支名可以不填写，所以参数1为分支简介
project_path=$(pwd)
echo $project_path
project_name="${project_path##*/}"
echo $project_name
# 父分支
if [ -z "$3" ];then
    parentBranch='maser-1'
else
    parentBranch=$3
fi
# 功能分支名
functionBranch=$2
# 分支简介
content=$1
# shell目录
shellDir=$(cd $(dirname $0); pwd)
# 项目目录
projectPath=$(pwd)
# 项目名
projectName="${projectPath##*/}"
# log存放目录
logFileBasePath="$shellDir/$projectName"
# 如果log目录不存在，则创建
if [ ! -d "$logFileBasePath" ];then
    mkdir $logFileBasePath
fi
# log文件路径
logFilePath="$logFileBasePath/git_branch.log"
# 分支简介必填
if [ -z $content ] ; then
    echo 'please enter content'
    exit 0
fi
# 如果没传分支名，设置默认分支名
if [ -z $functionBranch ] ; then
    functionBranch=$(date "+%Y%m%d-%H%M%S")
fi
# 切换到 parentBranch
git checkout $parentBranch
# 基于master 创建功能分支
branchName="feature/$functionBranch"
git checkout -b "$branchName"
# 将分支信息写入log
currentTime=$(date "+%Y-%m-%d %H:%M:%S")
# 想在这里输入当前所在目录，或者库
# 运行脚本的目录，也就是工作目录
workDirectory=$(pwd)
# 仓库地址
gitUrl=$(git remote get-url --push origin)
echo -e "$currentTime ($workDirectory)\r" >> $logFilePath
echo -e "$gitUrl add branch name：（ $branchName ） content：$content\r" >> $logFilePath
echo -e "\r" >> $logFilePath
echo '========== log start ============='
cat $logFilePath | tail -n 10
echo '========== log end ============='
