#!/usr/bin/env bash
#该脚本是获取deleteDirectory.txt文件中配置的目录，然后遍历每个目录，获取所有文件及其最后修改日期，并删除最后修改日期在15天之前的文件。

echo -e "\n\n"
directorys=$1
dayAgo=$2
#计算当前时间和要被删除的文件的最早的修改日期
today=`date +"%Y-%m-%d %H:%M:%S"`
echo -e "开始执行时间：${today}"
deleteDate=`date -d"-${dayAgo} day" +%Y-%m-%d`
echo -e "要删除的日志文件的最后修改日期：${deleteDate}\n"


#进入目录，然后删除文件的方法
function enterDirectory() {
    files=`ls $1`
    for file in ${files} ; do
        if [[ -f "$1/$file" ]]; then
            modifyDate=`stat "$1/${file}" | awk '{print $1}' |sed -n '6p'`
            modifyDate=${modifyDate##*：}
            if [[ $(echo ${modifyDate} ${deleteDate} | awk '$1<$2 {print -1} $1==$2 {print 0} $1>$2 {print 1}') -eq -1 ]]; then
                rm -f "$1/${file}"
                echo -e "$1/${file} 文件被删除"
            fi
        else if [[ -d "$1/$file" ]]; then
            enterDirectory "$1/$file"
        fi
        fi
    done
}

#遍历指定的目录，然后将目录下所有的文件按照指定规则删除
while read -r line
    do
        enterDirectory ${line}
    done < $directorys
